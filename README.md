# mpesa-c2b-payment-via-python3

Author: Miano Kariuki

Project purpose: To facilitate C2B payment of customer purchases on an online retail store. 

Transaction journey:

    1. Customer checkouts out of a merchant site after purchasing goods/services.

    2. The frontend service proceeds to submit a json request with: username and product id.

    3. The backend service proceeds to then validate whether the username and product id is valid. In the event that either info is invalid then NO_CUSTOMER_RECORD or NO_PRODUCT_RECORD response is given.

    4. If valid info is given for both username and product id, a mpesa transaction via Daraja is invoked with the relevant Customer and Product Details.

    5. Validate whether MPESA C2B transaction was successfully acknowledged and await for the MPESA C2B callback.

    6. If either the acknowledge failed or the transaction was rejected on C2B callback, provide the FAILED_TRANSACTION response.

    7. In the event C2B callback showcases successful transaction, save transaction in database and provide the SUCCESS response with mpesa transaction id.

    8. Optional: provide a receipt to the customer via email.


Prerequisite:

    1. A remote database exists with the tables Customer, Products and Transactions ---> reference instructions below for dockerized mysql db instance if this prerequisite is not meet.


Dockerize MYSQL DB locally:

	1. Create MySQL container via command "sudo docker run -p 3307:3306 --name=Python3_C2B_DB -e MYSQL_ROOT_PASSWORD=password -d mysql/mysql-server:latest"

	2. Verify that the container has been created via command "docker ps -a" 

	3. Connect to the container via command "docker exec -it Python3_C2B_DB /bin/bash"

	4. Login to the MySQL server via command "mysql -uroot -p -A" and use the MYSQL_ROOT_PASSWORD password provided which was "password"

	5. Using command "select user,host from mysql.user;", you can observe that the host is "localhost". There's need to change this value as it restricts us from accessing the server outside of the docker image.

	6. Update the root host value with command "update mysql.user set host='%' where user='root';" and update privileges via "flush privileges;"

	7. Exit from both container and mysql via command "exit" and connect to the db remotely using command " mysql -uroot -p -h127.0.0.1 -P3307"

	8. Perform house keeping rules:
		
		i) Create the database we will use under the name "TestDB" via command "create database TestDB;"

		ii) Create a user named "c2bUser" that the application will use to perform CRUD operations via command "CREATE USER 'c2bUser'@'%' IDENTIFIED BY 'password';". Verify creation using "select user,host from mysql.user;"

		iii) Restrict priviliges of c2bUser to only performing CRUD operations for TestDB database via commands respectively:

			- "REVOKE ALL PRIVILEGES ON *.* FROM 'c2bUser'@'%';" to revoke all permissions.

			- "GRANT ALL PRIVILEGES ON ON TestDB.* TO 'c2bUser'@'%';" to provide CRUD permissions to TestDB.


		iv) Create TestDB tables:

			- Customers(username, email, msisdn, createdOn, updatedOn)

				CREATE TABLE TestDB.Customers (
				    username VARCHAR(32) PRIMARY KEY,
				    email VARCHAR(64) NULL,
				    msisdn BIGINT NOT NULL CHECK (LENGTH(msisdn) = 12), 
				    createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
				    updatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
				);			

			- Products (id, name, description, price, createdOn, updatedOn)

				CREATE TABLE TestDB.Products (
				    id VARCHAR(64) PRIMARY KEY,
				    name VARCHAR(16) NOT NULL,
				    description VARCHAR(128) NULL,
				    price DECIMAL(10, 2) NOT NULL,
				    createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
				    updatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
				);

			- Transactions(id, msisdn, productID, mpesaTrxID, status, description, createdOn, updatedOn)

				CREATE TABLE TestDB.Transactions (
				    id INT AUTO_INCREMENT PRIMARY KEY,
					msisdn BIGINT NOT NULL CHECK (LENGTH(msisdn) = 12),
				    productID VARCHAR(64) NOT NULL,
					mpesaTrxID VARCHAR(32) NULL,
					status VARCHAR(8) NULL,
					description VARCHAR(64) NULL,
				    createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
				    updatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
				);			

		v) Logout via 'exit' command and login as 'c2bUser' and add some data in Customers and Products tables.

    

    

