from enum import Enum

class Scenario(Enum):

    SUCCESS = {"httpCode": 201, "responseCode": 4000, "responseMsg": "success", "detailedMsg": "Transaction completed successfully"}
    PROCESSING = {"httpCode": None, "responseCode": None, "responseMsg": "processing", "detailedMsg": "Transaction is currently processing"}
    NO_CUSTOMER_RECORD = {"httpCode": 400, "responseCode": 4001, "responseMsg": "failure", "detailedMsg": "The provided Username has no related customer record"}
    NO_PRODUCT_RECORD = {"httpCode": 400, "responseCode": 4002, "responseMsg": "failure", "detailedMsg": "The provided ProductID has no related product record"}
    MPESA_REQUEST_FAILED = {"httpCode": 500, "responseCode": 4003, "responseMsg": "failure", "detailedMsg": "MPESA C2B request has failed or been rejected"}
