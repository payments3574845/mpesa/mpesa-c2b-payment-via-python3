from django.db import models

# Create your models here.

class Customers(models.Model):

	username = models.CharField(max_length=32, primary_key=True)
	email = models.CharField(max_length=64)
	msisdn = models.IntegerField()
	createdOn = models.DateTimeField()
	updatedOn = models.DateTimeField()

	class Meta:
		managed = False
		db_table = "Customers"



class Products(models.Model):

	id = models.CharField(max_length=64, primary_key=True)
	name = models.CharField(max_length=16)
	description = models.CharField(max_length=128)
	price = models.FloatField()
	createdOn = models.DateTimeField()
	updatedOn = models.DateTimeField()

	class Meta:
		managed = False
		db_table = "Products"


