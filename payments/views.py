from rest_framework.decorators import api_view
from payments.scenario import Scenario
from payments.models import Customers
from payments.models import Products
from django.http import HttpResponse
from django.shortcuts import render
import requests
import json


import logging

@api_view(['POST'])
def postTransaction(request):
	
	logger = logging.getLogger(__name__)

	# Starting process:

	response = Scenario.PROCESSING.value
	mpesaTrxID = ""

	logger.info(" TransactionID = {} | Transaction = {} | Process = {} | ProcessStatus = {} | RequestPayload = {} | RequestHeaders = {} "
		.format(request.data.get("TransactionID"), "C2B Payment", "payments.views.postTransaction", "Starting Process", request.data, ""))


	# Fetch customer record from database:

	customer = Customers.objects.filter(username=request.data.get("Username"))

	logger.info(" TransactionID = {} | Transaction = {} | Process = {} | ProcessStatus = {} | CustomerRecord = {}"
		.format(request.data.get("TransactionID"), "C2B Payment", "payments.views.postTransaction", "Fetched customer record", customer))


	# Fetch product record from database:

	product = Products.objects.filter(id=request.data.get("ProductID"))

	logger.info(" TransactionID = {} | Transaction = {} | Process = {} | ProcessStatus = {} | ProductRecord = {}"
		.format(request.data.get("TransactionID"), "C2B Payment", "payments.views.postTransaction", "Fetched product record", product))


	# Validate whether both customer and product records are present: 

	if bool(customer) and bool(product):

		# Get Access Token from Daraja:

		accessTokenResponse = requests.get('https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials', 
									headers={"Authorization":"Basic U2F6QUZnSTRIb0dDVng1dEtKVTF0NE83R2RYZ1JIZlpWdGE0RFE1Tm1vTzZGVUcyOklVYlo4b3JVb3FDRUEzMzlFbHc1aXdtVWNoVVJzOHpXNVJBSjZtczBUQlJMNU9KYzBBbUFJTzZreTVpOHZCeUc="})

		accessTokenResponseJson = accessTokenResponse.json()

		logger.info(" TransactionID = {} | Transaction = {} | Process = {} | ProcessStatus = {} | AccessTokenResponse = {}"
			.format(request.data.get("TransactionID"), "C2B Payment", "payments.views.postTransaction", "Fetched Access Token from Daraja",
			accessTokenResponse))


		# Send HTTP C2B request to MPESA:

		jsonPayload = {
			'BusinessShortCode' : 174379, 
			'Password' : 'MTc0Mzc5YmZiMjc5ZjlhYTliZGJjZjE1OGU5N2RkNzFhNDY3Y2QyZTBjODkzMDU5YjEwZjc4ZTZiNzJhZGExZWQyYzkxOTIwMjQwMjAzMTgwNzU0',
			'Timestamp': '20240203180754', 
			'TransactionType': 'CustomerPayBillOnline', 
			'Amount': 1, 
			'PartyA': 254792651659,
			'PartyB': 174379, 
			"PhoneNumber": 254792651659,
			'CallBackURL': 'https://mydomain.com/path',
			'AccountReference': 'CompanyXLTD',
			'TransactionDesc': 'Payment of X'
		}

		c2bResponse = requests.post(
			'https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest', 
			headers={"Authorization": "Bearer {}".format(accessTokenResponseJson['access_token'])},
			json=jsonPayload
		)

		c2bResponseJson = c2bResponse.json()

		logger.info(" TransactionID = {} | Transaction = {} | Process = {} | ProcessStatus = {} | C2BRequestPayload = {} | C2BResponse = {}"
			.format(request.data.get("TransactionID"), "C2B Payment", "payments.views.postTransaction", "Sent NI Push Request", 
				json.dumps(jsonPayload), c2bResponseJson))

		if "ResponseCode" in c2bResponseJson and c2bResponseJson['ResponseCode'] == '0':

			response = Scenario.SUCCESS.value

		else:

			response = Scenario.MPESA_REQUEST_FAILED.value


	elif not bool(customer):

		response = Scenario.NO_CUSTOMER_RECORD.value

	else:
		
		response = Scenario.NO_PRODUCT_RECORD.value


	# Log the response:

	logger.info(" TransactionID = {} | Transaction = {} | Process = {} | ProcessStatus = {} | Response = {} | MpesaTrxID = {}"
		.format(request.data.get("TransactionID"), "C2B Payment", "payments.views.postTransaction", "Finished process", response, mpesaTrxID))


	return HttpResponse(status = response.get("httpCode"), content = json.dumps(
						{
							"ServiceResponse": {
								"Header": {
									"ResponseRefID": request.data.get("TransactionID"),
									"ResponseCode": response.get("responseCode"),
									"ResponseMsg": response.get("responseMsg"),
									"DetailedMsg": response.get("detailedMsg")
								},
								"Body": {
									"mpesaTrxID": mpesaTrxID
								}
							}
						}), content_type='application/json'
				)












